package Task1;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class YourVerticleSolution extends AbstractVerticle {

    /**
     *  Create your first verticle.
     *  1. Use AbstractVerticle to implement method "start"
     *  2. Every one second print "Hi!" on console
     *      2a) Use "setPeriodic" method in vertx object
     *  3. Run main class Task1Main. Please check which class is deployed!
     */


    @Override
    public void start() {
        vertx.setPeriodic(1000, handler -> {
            System.out.println("Hi!");
        });
    }

    /**
    // "Future" represents the result of an action that may, or may not, have occurred yet.
    @Override
    public void start(Future<Void> startFuture) throws Exception {
        super.start(startFuture);

        vertx.setPeriodic(1000, handler -> {
            System.out.println("Hi!");
        });
    }
     **/
}

package Task1;

public class YourVerticle {

    /**
     *  Create your first verticle.
     *  1. Use AbstractVerticle to implement method Start
     *  2. Every one second print "Hi!" on console
     *      2a) use "setPeriodic" method in vertx object
     *      2b) https://vertx.io/docs/kdoc/vertx/io.vertx.reactivex.core/-vertx/set-periodic.html
     */

    /**
     * Good to know:
     *
     * The Vert.x APIs are largely event driven.
     * This means that when things happen in Vert.x that you are interested in,
     * Vert.x will call you by sending you events.
     *
     * You handle events by providing handlers to the Vert.x APIs.
     * Some time later when Vert.x has an event to pass to your handler Vert.x will call it asynchronously.
     */
}

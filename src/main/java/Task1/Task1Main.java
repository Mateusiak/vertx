package Task1;

import io.vertx.core.Vertx;

public class Task1Main {
    public static void main(String[] args) {
        Vertx.vertx().deployVerticle(YourVerticleSolution.class.getName());
    }
}

package Task2;

public class YourMongoVerticle {

    /**
     *
     * Lookup in YourMongoVerticleSolution class!
     *
     *  1. Create verticle that will be responsible for creating mongodb client.
     *
     *  2. Use mongo client with config : classes needed - JsonObject, MongoClient
     *      2a. Put "connection_string" key with "mongodb://127.0.0.1:27018" value and
     *          put "db_name" key with "test" value into object of JsonObject class.
     *
     *          e.g.         JsonObject config = new JsonObject();
     *                      config.put("connection_string", "mongodb://127.0.0.1:27018" );
     *
     *      2b. Create MongoClient with config that was defined as JsonObject in 2a.
     *      2c. MongoClient dbClient = MongoClient.create(vertx, config);
     *  3. Create collections "languages". e.g. implement private void createCollectionLanguages(MongoClient dbClient)
     *
     *  4. Insert data to collections "languages" using insert method in mongo client. e.g. implement private void insertDocumentIntoCollectionLanguages(MongoClient dbClient)
     *
     *  5. Update data e.g. implement private void updateKotlinDescInCollectionLanguages(MongoClient dbClient)
     *      5a. Use "updateCollection" method, empty JsonObject, and below update object
     *                  JsonObject update = new JsonObject().put("$set", new JsonObject()
     *                 .put("kotlin", "kotlin is jvm lang"));
     *
     * 6. User helper method e.g. private void printLanguagesCollectionContent(MongoClient dbClient)
     *      6a. use method "find" in mongo client with with JsonObject query = new JsonObject();
     *      6b. When res.succeeded() is true, include code:
     *              for (JsonObject json : res.result()) {
     *                     System.out.println(json.encodePrettily());
     *              }
     *
     *
     *  !!! LOOKUP AT YourMongoVerticleSolution class to see code and try to reproduce that code here. !!!
     *
     *  Good to introduction to mongo client can be found:
     *  https://vertx.io/docs/vertx-mongo-client/java/
     */

}

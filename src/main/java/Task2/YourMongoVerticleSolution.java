package Task2;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;

public class YourMongoVerticleSolution extends AbstractVerticle {

    /**
     *  1. Create verticle.
     *  2. Use Mongo client with config : JsonObject, MongoClient
     *  3. Create methods:
     *      3.1. private void createCollectionLanguages(MongoClient dbClient)
     *      3.2. private void insertDocumentIntoCollectionLanguages(MongoClient dbClient)
     *      3.3. private void updateKotlinDescInCollectionLanguages(MongoClient dbClient)
     *      3.4. private void printLanguagesCollectionContent(MongoClient dbClient)
     */

    public void start(Promise<Void> promise) {

        JsonObject config = new JsonObject();
        config
            .put("connection_string", "mongodb://127.0.0.1:27018")
            .put("db_name", "test");

        // Mongo client resolves provided config. There are many key-value setting that can be used with mongo client
        // in resources/config there is example json file with key-value config
        MongoClient dbClient = MongoClient.create(vertx, config);

        createCollectionLanguages(dbClient);

        insertDocumentIntoCollectionLanguages(dbClient);

        updateKotlinDescInCollectionLanguages(dbClient);

        // check without timer what result will be printed on console about kotlin
        vertx.setPeriodic(2000, handler -> {
            printLanguagesCollectionContent(dbClient);
        });
    }

    private void updateKotlinDescInCollectionLanguages(MongoClient dbClient) {
        // update "kotlin" description in "languages" collection
        JsonObject originQuery = new JsonObject();
        // Set the author field
        JsonObject update = new JsonObject().put("$set", new JsonObject()
                .put("kotlin", "kotlin is jvm lang"));

        dbClient.updateCollection("languages", originQuery, update, res -> {
            if (res.succeeded()) {
                System.out.println("Kotlin updated !");
            } else {
                System.out.println(res.cause().getMessage());
            }
        });
    }

    private void insertDocumentIntoCollectionLanguages(MongoClient dbClient) {
        // insert some data about languages
        JsonObject document = new JsonObject()
                .put("java", "java is a coffee")
                .put("python", "python is a snake")
                .put("kotlin", "kotlin is a ketchup");

        dbClient.insert("languages", document, stringAsyncResult -> {
            if (stringAsyncResult.succeeded()) {
                System.out.println("Inserted: java, python, kotlin.");
            } else {
                System.out.println("Not inserted: " +  stringAsyncResult.cause());
            }
        });
    }

    private void createCollectionLanguages(MongoClient dbClient) {
        // create collection
        dbClient.createCollection("languages", voidAsyncResult -> {
            if (voidAsyncResult.succeeded()) {
                System.out.println("Collection languages created.");
            } else {
                System.out.println("Collection languages not created." + voidAsyncResult.cause());
            }
        });
    }

    private void printLanguagesCollectionContent(MongoClient dbClient) {
        // print all data in collection languages
        JsonObject query = new JsonObject();
        // if query empty object is passed in "find" there is no filtering criteria, all existing data
        // in "languages" collection will be returned
        dbClient.find("languages", query, res -> {
            if (res.succeeded()) {
                for (JsonObject json : res.result()) {
                    System.out.println("***\n" + json.encodePrettily());
                }
            } else {
                System.out.println(res.cause().getMessage());
            }
        });
    }
}

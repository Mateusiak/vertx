package Task2;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.embeddedmongo.EmbeddedMongoVerticle;

public class Task2MongoMain {

    public static void main(String[] args) {

        /**
         *  Below deployment config is required to run Embedded Mongo Db;
         *  Option 1. Deploy EmbeddedMongoVerticle and use mongo client with localhost
         *  Option 2. If this fails for your environment settings, please checkout to branch "cloud".
         *            We connect there to remote Mongo DB instance.
         */

        Vertx.vertx().deployVerticle(new EmbeddedMongoVerticle(), new DeploymentOptions()
                .setConfig(new JsonObject()
                        .put("port", 27018)
                        .put("host", "localhost")
                        .put("version", "3.4.2"))
                .setWorker(true));

        Vertx.vertx().deployVerticle(YourMongoVerticleSolution.class.getName());
    }
}

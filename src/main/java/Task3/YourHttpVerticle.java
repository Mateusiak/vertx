package Task3;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Purpose for Task 3 is to try REST with Vert.X
 * Mainly, see how to setup HTTP SERVER and API
 * Establish Mongo Client and REST service.
 * Here we operate in within one verticle. In real life, code is more complex
 * and we use Event bus to be a proxy between Verticles. That is shown in WorkshopEmbeddedMongo directory.
 *
 */
public class YourHttpVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(YourHttpVerticle.class);
    private MongoClient dbClient;

    @Override
    public void start(Promise<Void> promise) {
        // once mongo client will be set up, we start http server
        Future<Void> steps = createMongoClient().compose(v -> createServer());
        steps.setHandler(promise);
    }

    private Future<Void> createMongoClient() {
        Promise<Void> promise = Promise.promise();
        JsonObject config = new JsonObject();
        config
            .put("connection_string", "mongodb://127.0.0.1:27018")
            .put("db_name", "test");

        dbClient = MongoClient.create(vertx, config);

        // to check if client is available
        dbClient.getCollections(handler -> {
            if(handler.succeeded()) {
                LOGGER.info("Mongo Client works.");
                promise.complete();
            } else {
                LOGGER.error("Mongo Client cannot connect to mongo.", handler.cause());
                promise.fail("Cannot connect to mongo.");
            }
        });
        return promise.future();
    }

    private Future<Void> createServer() {
        Promise<Void> promise = Promise.promise();

        HttpServer server = vertx.createHttpServer();

        Router router = Router.router(vertx);

        /**
         * TODO
         *
         * Lookup in YourHttpVerticleSolution! :)
         *
         * Add endpoint to create collection
         * Add endpoint to get all collections
         * Add endpoint to delete collection
         */


        // ":name" is a parameter that is passed to handler
        router.post("/collection/:name").handler(this::createCollection);
        router.get("/collections").handler(this::getCollections);
        router.delete("/collection/:name").handler(this::deleteCollection);

        int portNumber = 8080;
        server
            .requestHandler(router)
            .listen(portNumber, ar -> {
                if (ar.succeeded()) {
                    promise.complete();
                    LOGGER.info("Server started.");
                } else {
                    promise.fail(ar.cause());
                    LOGGER.error("Server failed.");
                }
            });

        return promise.future();
    }

    private void deleteCollection(RoutingContext routingContext) {
        /*
            TODO-3
         */
    }

    private void createCollection(RoutingContext routingContext) {
        /*
            TODO-1
         */
    }

    private void getCollections(RoutingContext routingContext) {
        /*
            TODO-2
         */
    }
}

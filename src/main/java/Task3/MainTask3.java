package Task3;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.SLF4JLogDelegateFactory;
import io.vertx.ext.embeddedmongo.EmbeddedMongoVerticle;

public class MainTask3 {

    public static void main(String[] args) {

        String logFactory = System.getProperty("org.vertx.logger-delegate-factory-class-name");
        if (logFactory == null) {
            System.setProperty("org.vertx.logger-delegate-factory-class-name", SLF4JLogDelegateFactory.class.getName());
        }

        Vertx.vertx().deployVerticle( new EmbeddedMongoVerticle(), new DeploymentOptions()
                .setConfig( new JsonObject()
                        .put("port", 27018)
                        .put("host", "localhost")
                        .put("version", "3.4.2"))
                .setWorker(true));

        Vertx.vertx().deployVerticle(YourHttpVerticleSolution.class.getName());

    }
}

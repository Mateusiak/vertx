package Task3;

import Task2.YourMongoVerticleSolution;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class YourHttpVerticleSolution extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(YourMongoVerticleSolution.class);
    private MongoClient dbClient;

    @Override
    public void start(Promise<Void> promise) {
        Future<Void> steps = createMongoClient().compose(v -> createServer());
        steps.setHandler(promise);
    }

    private Future<Void> createMongoClient() {
        Promise<Void> promise = Promise.promise();
        JsonObject config = new JsonObject();
        config
                .put("connection_string", "mongodb://127.0.0.1:27018")
                .put("db_name", "test");

        dbClient = MongoClient.create(vertx, config);

        dbClient.getCollections(handler -> {
            if(handler.succeeded()) {
                promise.complete();
            } else {
                promise.fail("Cannot connect to mongo.");
            }
        });
        return promise.future();
    }

    private Future<Void> createServer() {
        Promise<Void> promise = Promise.promise();
        HttpServer server = vertx.createHttpServer();

        Router router = Router.router(vertx);

        /* Add endpoint to create collection
         * Add endpoint to get all collections
         * Add endpoint to delete collection
         */

        // for test: in Postman POST http://localhost:8080/collection/languages
        router.post("/collection/:name").handler(this::createCollection);

        // for test: in Postman GET http://localhost:8080/collections
        router.get("/collections").handler(this::getCollections);

        //fot test: in Postman DELETE http://localhost:8080/collection/languages
        router.delete("/collection/:name").handler(this::deleteCollection);

        int portNumber = 8080;
        server
            .requestHandler(router)
            .listen(portNumber, ar -> {
                if (ar.succeeded()) {
                    promise.complete();
                    LOGGER.info("Server started. http://localhost:8080/collections");
                } else {
                    promise.fail(ar.cause());
                    LOGGER.error("Server failed.");
                }
            });

        return promise.future();
    }

    private void deleteCollection(RoutingContext routingContext) {
        String collectionName = routingContext.request().getParam("name");

        if (collectionName.isEmpty()) {
            routingContext.fail( new RuntimeException("empty param"));
        }

        dbClient.dropCollection(collectionName, collectionHandler -> {
            if (collectionHandler.succeeded()) {
                System.out.println("Collection " + collectionName + " dropped");
                routingContext.put("data", collectionHandler.result());
                routingContext.response().end( "Collection " + collectionName +  " dropped.");
            } else {
                System.out.println("Collection " + collectionName + "not created.");
                routingContext.fail(collectionHandler.cause());
            }
        });
    }

    private void createCollection(RoutingContext routingContext) {
        String collectionName = routingContext.request().getParam("name");

        if (collectionName.isEmpty()) {
            routingContext.fail( new RuntimeException("empty param"));
        }

        dbClient.createCollection(collectionName, collectionHandler -> {
            if (collectionHandler.succeeded()) {
                System.out.println("Collection " + collectionName + " created");
                routingContext.put("data", collectionHandler.result());
                routingContext.response().end( "collection created.");
            } else {
                System.out.println("Collection " + collectionName + "not created.");
                routingContext.fail(collectionHandler.cause());
            }
        });
    }

    private void getCollections(RoutingContext routingContext) {
        dbClient.getCollections( handler -> {
            if (handler.succeeded()) {
                System.out.println("Collection that exists: " + handler.result().toString());
                routingContext.put("data", handler.result().toString());
                routingContext.response().end(handler.result().toString());
            } else {
                routingContext.fail(handler.cause());
                System.out.println("Can't get collections:" + handler.cause());
            }
        });
    }
}

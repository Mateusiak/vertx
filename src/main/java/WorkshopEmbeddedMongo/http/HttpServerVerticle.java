package WorkshopEmbeddedMongo.http;

import WorkshopEmbeddedMongo.database.MongoDatabaseService;
import io.vertx.core.*;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpServerVerticle extends AbstractVerticle {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServerVerticle.class);
    private MongoDatabaseService dbService;

    @Override
    public void start(Promise<Void> promise) {
        String mongodb = "mongo-db";
        dbService = MongoDatabaseService.createProxy(vertx, mongodb);

        HttpServer server = vertx.createHttpServer();

        Router router = Router.router(vertx);
        router.get("/collections").handler(this::getCollections);
        router.post("/collection/:name").handler(this::createCollection);
        router.delete("/collection/:name").handler(this::deleteCollection);

        int portNumber = 8080;

        server
            .requestHandler(router)
            .listen(portNumber, ar -> {
                if (ar.succeeded()) {
                    promise.complete();
                    LOGGER.info("Server started.");
                } else {
                    promise.fail(ar.cause());
                    LOGGER.error("Server failed.");
                }
            });
    }

    private void deleteCollection(RoutingContext routingContext) {
        String collectionName = routingContext.request().getParam("name");

        if (collectionName.isEmpty()) {
            routingContext.fail(new RuntimeException("empty param"));
        }

        dbService.dropCollection(collectionName, collectionHandler -> {
            if (collectionHandler.succeeded()) {
                System.out.println("Collection " + collectionName + " dropped");
                routingContext.put("data", collectionHandler.result());
                routingContext.response().end("collection" + collectionName + "dropped.");

            } else {
                System.out.println("Collection " + collectionName + "not created.");
                routingContext.fail(collectionHandler.cause());
            }
        });
    }

    private void createCollection(RoutingContext routingContext) {
        String collectionName = routingContext.request().getParam("name");

        if (collectionName.isEmpty()) {
            routingContext.fail(new RuntimeException("empty param"));
        }

        dbService.createCollection(collectionName, collectionHandler -> {
            if (collectionHandler.succeeded()) {
                System.out.println("Collection " + collectionName + " created");
                routingContext.put("data", collectionHandler.result());
                routingContext.response().end("collection created.");

            } else {
                System.out.println("Collection " + collectionName + "not created.");
                routingContext.fail(collectionHandler.cause());
            }
        });
    }

    private void getCollections(RoutingContext routingContext) {
        dbService.getCollections(handler -> {
            if (handler.succeeded()) {
                System.out.println("Collection that exists: " + handler.result().toString());
                routingContext.put("data", handler.result().toString());
                routingContext.response().end(handler.result().toString());
            } else {
                routingContext.fail(handler.cause());
                System.out.println("Can't get collections:" + handler.cause());
            }
        });
    }
}
package WorkshopEmbeddedMongo;

import WorkshopEmbeddedMongo.database.MongoVerticle;
import WorkshopEmbeddedMongo.http.HttpServerVerticle;
import io.vertx.core.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainVerticle extends AbstractVerticle
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);

    @Override
    public void start(Promise<Void> promise) throws Exception {
        LOGGER.info("Start method.");

        vertx.deployVerticle(new MongoVerticle(), res -> {
            if (res.failed()) {
                LOGGER.info(res.cause().getMessage());
                LOGGER.error("Failed to deploy verticle ");
            } else {
                LOGGER.info("Deployed mongo client verticle! ");
                vertx.deployVerticle(
                        new HttpServerVerticle(),
                        new DeploymentOptions().setInstances(1), responseHttp -> {
                            if (responseHttp.succeeded()) {
                                LOGGER.info("Done!");
                                LOGGER.info("Open browser: http://localhost:8080/collections");
                            } else {
                                LOGGER.error(responseHttp.cause().getMessage());
                            }
                        });
            }
        });
    }
}
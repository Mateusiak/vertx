package WorkshopEmbeddedMongo.database;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;

import java.util.List;

public class MongoDatabaseServiceImpl implements MongoDatabaseService {

    private MongoClient mongo;

    public MongoDatabaseServiceImpl(MongoClient mongoClient, Handler<AsyncResult<MongoDatabaseService>> readyHandler) {
        this.mongo = mongoClient;
        mongo.getCollections( r -> {

        if (r.failed()) {
            readyHandler.handle(Future.failedFuture(r.cause()));
        } else {
            readyHandler.handle(Future.succeededFuture(this));
        } });
    }

    @Override
    public MongoDatabaseService getCollections(Handler<AsyncResult<JsonObject>> handler) {
        mongo.getCollections(fetch -> {
            if (fetch.succeeded()) {
                JsonObject response = new JsonObject();
                List<String> resultSet =  fetch.result();
                if (resultSet.size() == 0) {
                    response.put("found", false);
                } else {
                    response.put("found", true);
                    response.put("rawContent", resultSet.toString());
                }
                handler.handle(Future.succeededFuture(response));
            } else {
                handler.handle(Future.failedFuture(fetch.cause()));
            }
        });
        return this;
    }

    @Override
    public MongoDatabaseService dropCollection(String collectionName, Handler<AsyncResult<JsonObject>> handler) {
        mongo.dropCollection(collectionName, fetch -> {
            JsonObject response = new JsonObject();

            if (fetch.succeeded()) {
                response.put("deleted", true);
                handler.handle(Future.succeededFuture(response));
            } else {
                response.put("deleted", false);
                response.put("rawContent", fetch.cause().getMessage());
                handler.handle(Future.failedFuture(fetch.cause()));
            }
        });
        return this;
}

    @Override
    public MongoDatabaseService createCollection(String collectionName, Handler<AsyncResult<JsonObject>> handler) {
        mongo.createCollection(collectionName, fetch -> {
            JsonObject response = new JsonObject();

            if (fetch.succeeded()) {
                response.put("created", true);
                handler.handle(Future.succeededFuture(response));
            } else {
                response.put("created", false);
                response.put("rawContent", fetch.cause().getMessage());
                handler.handle(Future.failedFuture(fetch.cause()));
            }
        });
        return this;
    }
}
package WorkshopEmbeddedMongo.database;

import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.GenIgnore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;

/**
 * https://vertx.io/docs/vertx-service-proxy/java/
 *
 * You write your service as a Java interface and annotate it with the @ProxyGen annotation
 * Given the interface, Vert.x will generate all the boilerplate code required to access your service over the event bus,
 * and it will also generate a client side proxy for your service, so your clients can use a rich idiomatic API
 * for your service instead of having to manually craft event bus messages to sen
 *
 * You can also combine @ProxyGen with language API code generation (@VertxGen) in order to create service stubs
 * in any of the languages supported by Vert.x - this means you can write your service once in Java
 * and interact with it through an idiomatic other language API irrespective of whether the service
 * lives locally or is somewhere else on the event bus entirely
 *
 *
 * Service annotated with @ProxyGen annotation trigger the generation of the service helper classes:
 * The service proxy: a compile time generated proxy that uses the EventBus to interact with the service via messages
 * The service handler: a compile time generated EventBus handler that reacts to events sent by the proxy
 *
 */

@ProxyGen
@VertxGen
public interface MongoDatabaseService {

    @GenIgnore
    static MongoDatabaseService create(MongoClient mongoClient, Handler<AsyncResult<MongoDatabaseService>> readyHandler){
        return new MongoDatabaseServiceImpl(mongoClient, readyHandler);
    }

    @Fluent
    MongoDatabaseService getCollections(Handler<AsyncResult<JsonObject>> handler);

    @Fluent
    MongoDatabaseService dropCollection(String collectionName, Handler<AsyncResult<JsonObject>> handler);

    @Fluent
    MongoDatabaseService createCollection(String collectionName, Handler<AsyncResult<JsonObject>> handler);

    @GenIgnore
    static MongoDatabaseService createProxy(Vertx vertx, String address) {
        // this class is generated in main/generated folder
        return new MongoDatabaseServiceVertxEBProxy(vertx, address);
    }

}
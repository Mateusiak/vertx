package WorkshopEmbeddedMongo.database;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.serviceproxy.ServiceBinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MongoVerticle  extends AbstractVerticle {
    private static final String ADDRESS = "mongo-db";
    private static final Logger LOGGER = LoggerFactory.getLogger(MongoVerticle.class);

    public void start(Promise<Void> promise) throws Exception {
        LOGGER.info("In mongo verticle start.");
        super.start();
        JsonObject config = new JsonObject();
        config
            .put("connection_string", "mongodb://127.0.0.1:27018")
            .put("db_name", "test");

        MongoClient dbClient = MongoClient.create(vertx, config);

        MongoDatabaseService.create(dbClient, ready -> {
            if (ready.succeeded()) {
                ServiceBinder binder = new ServiceBinder(vertx);
                binder.setAddress(ADDRESS).register(MongoDatabaseService.class, ready.result());
                promise.complete();
            } else {
                promise.fail(ready.cause());
            }
        });
    }
}
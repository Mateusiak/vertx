package WorkshopEmbeddedMongo;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.SLF4JLogDelegateFactory;
import io.vertx.ext.embeddedmongo.EmbeddedMongoVerticle;

/**
 * About embedded Mongo:
 * https://github.com/vert-x3/vertx-embedded-mongo-db/blob/3.6/src/test/java/io/vertx/ext/embeddedmongo/test/EmbeddedMongoVerticleTest.java
 */
public class Main {

    public static void main(String[] args) {

        String logFactory = System.getProperty("org.vertx.logger-delegate-factory-class-name");
        if (logFactory == null) {
            System.setProperty("org.vertx.logger-delegate-factory-class-name", SLF4JLogDelegateFactory.class.getName());
        }

        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle( new EmbeddedMongoVerticle(), new DeploymentOptions()
                .setConfig( new JsonObject()
                        .put("port", 27018)
                        .put("host", "127.0.0.1")
                        .put("version", "3.4.2"))
                .setWorker(true), res -> {
            if (res.succeeded()) {
                vertx.deployVerticle(MainVerticle.class.getName());
            }
        });
    }
}
